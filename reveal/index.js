module.exports = {
    toolbar: {
        exports: ['pdf', 'publish',
            {
                type: 'reveal',
                icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAIAAACQkWg2AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gsUBjMcqKxj6wAAAYlJREFUKM/Fkb9L1WEYxc/zfr/v+35v3sQULxZBNRS4iKCooCAiCEZBSEQtTRFOObY4ObuEa4P4FwghDoo6OV6Xi0HoUsq1n3C7fH9/3+dpiMTwBjl5pmc5nOd8DiWHE/hfCUj7kOICBpDCBXVZBiIQtbgB+C2qCZJMICgFxIwoEXYILGlNct5AhDiV9Z08y+T5jH1/6Na282bIQ33+xIjWhlokpCk2d7MolkdT5s1yXPtQ3Lvjfaq7mz2qr7fVS0QwGoWjKMHJNx4bNK9flpIMNyqUZOL/cyTGtXYaHdCrG2mjyY+nbcn65fLfw3kKWtNvLI4l6FCzz0ovngSfv/PCUvhuK9X+GawiOP7C1VoeRhLG8DyEP3hvv7g/bhfm2tquqN1q7tyZDsyo1oqllXhsUH88dg8nzdGJW3wbVbrU9W4ljLu3PaXgzb+6ddo1sGj85PpXHu7XTx/YSqfq7FBHdddoyuiAnpkyV8s+JQfjf+jAV/AMJbEEARW5MMMYQJDmsJZcwYXTvwDcCqPLHy1HngAAAABJRU5ErkJggg==',
                tooltip: 'Reveal.js',
                format: 'reveal',
                process: function () {
                    electron.exportReveal('node_modules/reveal.js');
                }
            }],
        inlines: ['code', 'important', 'quote', 'link'],
        sections: ['normal', 'header', 'olist', 'ulist'],
        divisions: ['blockquote', 'blockcode'],
        inserts: ['image', 'attachment']
    },
    model: {
        decorators: ['code', 'important', 'quote', 'link'],
        sections: ['hierarchical', 'olist', 'ulist', 'image', 'attachment'],
        divisions: ['blockquote', 'blockcode']
    },
    language: 'FR',
};
